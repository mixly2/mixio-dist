# MixIO私有化部署版丨MixIO Community Server

#### 介绍
MixIO是Mixly团队推出的开源物联网应用开发平台，立足于信息技术教育行业，支持组件化的物联网项目定制、数据收发和逻辑编写，赋予用户快速构建自定义物联网应用的基础能力。平台自2021年5月起上线运行（ http://mixio.mixly.org ），现已拥有上千名注册用户。
基于中小学信息技术教室的基本需要，MixIO服务器的开源工作（MixIO Community Server）自2021年12月起正式启动，旨在为中小学教师提供一个可以快速启用的私有化MixIO服务端。2022年2月起，MixIO Community Server正式对外开放。

#### 软件架构
前端：JavaScript（Bootstrap+jQuery）
后端：Node.js (Express+Aedes)

#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx